import math

# ecuación lineal de segundo grado

def eq2(a, b, c):

    a = float(a)
    b = float(b)
    c = float(c)

    # esto es el discriminante
    disc = b**2 - 4*a*c

    if disc < 0:
        return None
    else:
        x1 = (-b + math.sqrt(disc))/2*a
        x2 = (-b - math.sqrt(disc))/2*a
    
        return [x1, x2]


def eq(a, b):

    a = float(a)
    b = float(b)
    
    if a == 0:
        return None
    else:
        return -b/a


